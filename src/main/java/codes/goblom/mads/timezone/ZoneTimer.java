/*
 * Copyright (C) Bryan Larson - All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential
 * 
 * Written by Bryan Larson Apr 6, 2020
 */
package codes.goblom.mads.timezone;

import codes.goblom.mads.api.Mads;
import codes.goblom.mads.api.ServerController;
import codes.goblom.mads.api.ServerType;
import codes.goblom.mads.api.sockets.SocketConnection;
import codes.goblom.mads.release.bridge.BridgeInfo;
import codes.goblom.mads.release.bridge.MadsBridge;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;

/**
 *
 * @author Bryan Larson
 */
public class ZoneTimer implements Runnable {
    private final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm"); //If add seconds add ':ss' to the format
    
    private final TimeZone timeZone;
    private final Condition condition;
    private final String sendPlayersTo;
    
    @Setter
    private boolean cancel = false;
    private boolean started = false;
    
    private final List<ServerController> createdControllers = new ArrayList();
    
    public ZoneTimer(TimeZone timeZone, Condition condition, String sendPlayersTo) {
        this.timeZone = timeZone;
        this.condition = condition;
        this.sendPlayersTo = sendPlayersTo;
        
        this.TIME_FORMAT.setTimeZone(timeZone);
    }
//    protected Date getFormattedTime(String str) throws ParseException {
//        DateFormat format;
//        
//        if (str.contains("am") || str.contains("AM") || str.contains("pm") || str.contains("PM")) {
//            format = new SimpleDateFormat("hh:mm a");
//        } else {
//            format = new SimpleDateFormat("hh:mm");
//        }
//        format.setTimeZone(timeZone);
//        return format.parse(str);
//    }
    
    @Override
    public void run() {
        if (cancel) return;
        
//        if (start == null) {
//            try {
//                start = getFormattedTime(condition.getStart());
//            } catch (Exception e) { 
//                e.printStackTrace();
//                cancel = true;
//            }
//        }
//        
//        if (end == null) {
//            try {
//                end = getFormattedTime(condition.getEnd());
//            } catch (Exception e) { 
//                e.printStackTrace();
//                cancel = true;
//            }
//        }
        
//        LocalTime now = LocalTime.now(ZoneId.of(timeZone.getID()));
        
        Date dNow = Calendar.getInstance(timeZone).getTime();
        String now = TIME_FORMAT.format(dNow);
        
        try {
            if (isTimeBetween(condition.getStart(), condition.getEnd(), now)) {
                if (!started) {
                    started = true;
                    long min = condition.getMinimum();
                    final ServerType type = Mads.getServerType(condition.getServerType());
                    
                    if (type == null) {
                        cancel = true;
                        throw new NullPointerException("ServerType " + condition.getServerType() + " does not exist.");
                    }
                    
                    long accounted = Mads.getAllControllers().stream().filter((cont) -> cont.getType().equals(type)).count();
                    
                    if (accounted < min) {
                        min = (int) (min - accounted);
                        
                        for (int i = 0; i < min; i++) {
                            String name = "TZA_" + type.getName() + "_" + i; //TZA_NAME_ID.... 
                                                                             // This makes sure that we dont try and reuse names that that user already specified by adding a prefix.
                            
                            ServerController cont = Mads.buildController().name(name).type(type.getName()).build();
                            
                            this.createdControllers.add(cont);
                        }
                    }
                    
                    started = true;
                }
            } else {
                if (started || !createdControllers.isEmpty()) {
                    //Destroy all servers.
                    started = false;
                    
                    for (ServerController cont : createdControllers) {
                        //TODO: Account to make sure it is safe to move players
                        //      At the moment, it just forces player back to lobby
                        
                        if (MadsBridge.isEnabled()) {
                            // Try to gracefully shutdown the servers.
                            SocketConnection sc = MadsBridge.getConnection(cont.getName());
                            if (sc != null) {
                                sc.send(BridgeInfo.TRY_SHUTDOWN);
                            }
                        } else {
                            ProxyServer.getInstance().getPlayers().stream()
                                    .filter((p) -> p.getServer().getInfo().getName().equals(cont.getName()))
                                    .forEach((p) -> p.connect(ProxyServer.getInstance().getServerInfo(sendPlayersTo)));
                        }
                        
                        ProxyServer.getInstance().getServers().remove(cont.getName());
                        
                        if (MadsBridge.isEnabled()) {
                            try {
                                Thread.sleep(TimeUnit.MINUTES.toMillis(2)); //Give the servers 2 minutes to close up shop before deletion
                            } catch (Exception e) { }
                        }
                        
                        Mads.removeController(cont.getName());
                    }
                }
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
            cancel = true;
        }
    }

    //https://stackoverflow.com/a/27564207
    //https://regex101.com/
    private static final String REGEX = "^([0-1][0-9]|2[0-3]):([0-5][0-9])"; //:([0-5][0-9])$"; //Commented code adds seconds
    private boolean isTimeBetween(String startTime, String endTime, String now) throws ParseException {
//        System.out.println("Start: " + startTime);
//        System.out.println("End: " + endTime);
//        System.out.println("Now: " + now);
        
        if (startTime.matches(REGEX) && endTime.matches(REGEX) && now.matches(REGEX)) {
            //Start Time
            //all times are from java.util.Date
            Date inTime = TIME_FORMAT.parse(startTime);
            Calendar calendar1 = Calendar.getInstance(timeZone);
            calendar1.setTime(inTime);

            //Current Time
            Date checkTime = TIME_FORMAT.parse(now);
            Calendar calendar3 = Calendar.getInstance(timeZone);
            calendar3.setTime(checkTime);

            //End Time
            Date finTime = TIME_FORMAT.parse(endTime);
            Calendar calendar2 = Calendar.getInstance(timeZone);
            calendar2.setTime(finTime);

            if (endTime.compareTo(startTime) < 0) {
                calendar2.add(Calendar.DATE, 1);
                calendar3.add(Calendar.DATE, 1);
            }

            java.util.Date actualTime = calendar3.getTime();
            
            if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0) && actualTime.before(calendar2.getTime())) {
                return true;
            }
        } else {
            throw new IllegalArgumentException("Not a valid time, expecting HH:mm format where HH is between 0-23 && mm is between 0-59");
        }
        
        return false;
    }
}
