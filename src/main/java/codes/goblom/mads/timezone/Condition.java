/*
 * Copyright (C) Bryan Larson - All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential
 * 
 * Written by Bryan Larson Apr 6, 2020
 */
package codes.goblom.mads.timezone;

import java.util.Map;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.md_5.bungee.config.Configuration;

/**
 *
 * @author Bryan Larson
 */
@RequiredArgsConstructor
@Getter
@ToString
public class Condition {
    private static final String KEY_START = "Start";
    private static final String KEY_END = "End";
    private static final String KEY_MINIMUM = "Maintain Minimum";
    private static final String KEY_TYPE = "Server Type";
    
    private final String start;
    private final long minimum;
    private final String serverType;
    private final String end;
    
    public Condition(Configuration config) {
        this.start = config.getString(KEY_START);
        this.minimum = config.getLong(KEY_MINIMUM);
        this.serverType = config.getString(KEY_TYPE);
        this.end = config.getString(KEY_END);
    }
    
    public Condition(Map<String, Object> map) {
        this.start = (String) map.get(KEY_START);
        this.minimum = (long) map.get(KEY_MINIMUM);
        this.serverType = (String) map.get(KEY_TYPE);
        this.end = (String) map.get(KEY_END);
    }
}
