/*
 * Copyright (C) Bryan Larson - All Rights Reserved
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential
 * 
 * Written by Bryan Larson Apr 6, 2020
 */
package codes.goblom.mads.timezone;

import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

/**
 *
 * @author Bryan Larson
 */
public class MadsTimezonePlugin extends Plugin {
    private Configuration config;
    
    private final List<ZoneTimer> timers = new ArrayList();
    
    @Override
    public void onEnable() {
        try {
            this.config = getConfiguration("config.yml");
        } catch (Exception e) {
            e.printStackTrace();
            
            getLogger().severe("There was an errror getting the config. Plugin not enabling.");
            return;
        }
        
        final String serverToSend = config.getString("On End Send Players To");
        final TimeZone timeZone = TimeZone.getTimeZone(ZoneId.of(config.getString("TimeZone")));
        
        List<Map<String, Object>> conditions = (List<Map<String, Object>>) config.getList("Conditions");
        List<Condition> list = Lists.transform(conditions, Condition::new);
        
        list.forEach((c) -> {
            ZoneTimer zt = new ZoneTimer(timeZone, c, serverToSend);
            
            timers.add(zt);
            
            ProxyServer.getInstance().getScheduler().schedule(this, zt, 1, 1, TimeUnit.MINUTES);
        });
        
    }
    
    public List<ZoneTimer> getTimers() {
        return new ArrayList(timers);
    }
    
    private Configuration getConfiguration(String name) throws IOException {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }

        File configFile = new File(getDataFolder(), name);
        if (!configFile.exists()) {
            try (InputStream in = getResourceAsStream(name)) {
                Files.copy(in, configFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
    }
}
